import 'package:flutter/material.dart';
import 'package:meta_economica/localizations/custom_dialog_meta_completa.i18n.dart';

class CustomDialogMetaCompleta extends StatefulWidget {

  CustomDialogMetaCompleta();

  @override
  _CustomDialogMetaCompletaState createState() => _CustomDialogMetaCompletaState();
}

class _CustomDialogMetaCompletaState extends State<CustomDialogMetaCompleta> {

  var primaryColor = const Color(0xFF753EE0);
  var segundaryColor = Colors.white;
  var terciaryColor = const Color(0xFFA581E7);
  
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Consts.padding),
      ),      
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return SingleChildScrollView(
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: Consts.avatarRadius + Consts.padding,
              bottom: Consts.padding,
              left: Consts.padding,
              right: Consts.padding,
            ),
            margin: EdgeInsets.only(top: Consts.avatarRadius),
            decoration: new BoxDecoration(
              color: Colors.white,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(Consts.padding),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 10.0,
                  offset: const Offset(0.0, 10.0),
                ),
              ],
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min, // To make the card compact
              children: <Widget>[
                Text(
                  "Parabéns!".i18n,
                  style: TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SizedBox(height: 16.0),
                Text(
                  "Voce conseguiu completar o seu objetivo!".i18n,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                ),
                SizedBox(height: 24.0)
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: Consts.avatarRadius,
              bottom: Consts.padding,
              left: Consts.padding,
              right: Consts.padding,
            ),
            child: Image(image: new AssetImage("assets/confete1.gif"),),
          ),
          Positioned(
            left: Consts.padding,
            right: Consts.padding,
            child: CircleAvatar(
              child: Image.asset('assets/icon.png'),
              backgroundColor: Colors.transparent,
              radius: Consts.avatarRadius,
            ),
          ),
        ],
      ),
    );
  }
}

class Consts {
  Consts._();

  static const double padding = 16.0;
  static const double avatarRadius = 66.0;
}
