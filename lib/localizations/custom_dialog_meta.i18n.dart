import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

    static var _t = Translations("pt_br") +
        {
          "pt_br": "Editando..",
          "en_us": "Editing..",
        } +
        {
          "pt_br": "Nova Meta",
          "en_us": "New Goal",
        } +
        {
          "pt_br": "Novo valor",
          "en_us": "New value",
        } +
        {
          "pt_br": "Motivo:",
          "en_us": "Reason:",
        } +
        {
          "pt_br": "Cancelar",
          "en_us": "Cancel",
        } +
        {
          "pt_br": "Preencha todos os campos!",
          "en_us": "Fill in all the fields!",
        } +
        {
          "pt_br": "Salvar",
          "en_us": "Save",
        };

  String get i18n => localize(this, _t);
}

