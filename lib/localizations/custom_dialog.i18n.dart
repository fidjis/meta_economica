import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

    static var _t = Translations("pt_br") +
        {
          "pt_br": "Valor",
          "en_us": "Value",
        } +
        {
          "pt_br": "Resgistro",
          "en_us": "Record",
        } +
        {
          "pt_br": "Cancelar",
          "en_us": "Cancel",
        } +
        {
          "pt_br": "Preencha todos os campos!",
          "en_us": "Fill in all the fields!",
        } +
        {
          "pt_br": "Salvar",
          "en_us": "Save",
        } +
        {
          "pt_br": "Comentario",
          "en_us": "Comment",
        } +
        {
          "pt_br": "    É um Saldo    ",
          "en_us": "  It's a Balance  ",
        } +
        {
          "pt_br": "É uma Retirada",
          "en_us": " It's a debt  ",
        };

  String get i18n => localize(this, _t);
}

