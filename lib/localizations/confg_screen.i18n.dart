import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

    static var _t = Translations("pt_br") +
        {
          "pt_br": "Configurações",
          "en_us": "Settings",
        } +
        {
          "pt_br": "Meta Econômica",
          "en_us": "Economic Goal",
        };

  String get i18n => localize(this, _t);
}

