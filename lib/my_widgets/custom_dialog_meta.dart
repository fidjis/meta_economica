import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:meta_economica/database/database_crud.dart';
import 'package:meta_economica/models/metaModel.dart';
import 'package:meta_economica/localizations/custom_dialog_meta.i18n.dart';

class CustomDialogMeta extends StatefulWidget {

  final String valorAtual;
  final bool isEditMode;
  final MetaModel dataRecebida;

  CustomDialogMeta({
    @required this.isEditMode,
    this.dataRecebida,
    this.valorAtual,
  });

  @override
  _CustomDialogMetaState createState() => _CustomDialogMetaState();
}

class _CustomDialogMetaState extends State<CustomDialogMeta> {
  
  DatabaseCRUD db = new DatabaseCRUD();
  final valorInputBoxController = TextEditingController();
  final motivoInputBoxController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black.withOpacity(0.6),
      body: Dialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(Consts.padding),
        ),      
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        child: dialogContent(context),
      ),
    );
  }

  dialogContent(BuildContext context) {
    return SingleChildScrollView(
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: Consts.avatarRadius + Consts.padding,
              bottom: Consts.padding,
              left: Consts.padding,
              right: Consts.padding,
            ),
            margin: EdgeInsets.only(top: Consts.avatarRadius),
            decoration: new BoxDecoration(
              color: Colors.white,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(Consts.padding),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 10.0,
                  offset: const Offset(0.0, 10.0),
                ),
              ],
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min, // To make the card compact
              children: <Widget>[
                Text(
                  widget.isEditMode ? "Editando..".i18n : "Nova Meta".i18n,
                  style: TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  widget.valorAtual ?? "",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  maxLength: 7, //limite de 9 milhoes
                  controller: valorInputBoxController,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    WhitelistingTextInputFormatter.digitsOnly
                  ],
                  decoration: new InputDecoration(
                    hintText: "Novo valor".i18n + " R\$",
                  ),
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  controller: motivoInputBoxController,
                  decoration: new InputDecoration(
                    hintText: "Motivo:".i18n,
                  ),
                ),
                SizedBox(height: 50.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop(); // To close the dialog
                      },
                      child: Text("Cancelar".i18n, style: TextStyle(color: Colors.red)),
                    ),
                    FlatButton(
                      onPressed: () async {
                        if(valorInputBoxController.text .isEmpty || motivoInputBoxController.text.isEmpty)
                          showDialog(
                            barrierDismissible: false,
                            context: context,
                            builder: (BuildContext context) {
                              // retorna um objeto do tipo Dialog
                              return AlertDialog(
                                //title: new Text("Atenção!"),
                                content: new Text("Preencha todos os campos!".i18n),
                                actions: <Widget>[
                                  new FlatButton(
                                    child: new Text("ok"),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              );
                            },
                          );
                        else{
                          MetaModel data;
                          if(widget.isEditMode){

                            data = widget.dataRecebida;
                            data.valor = valorInputBoxController.text;
                            data.motivo = motivoInputBoxController.text;

                            await db.atualizarMetas(widget.dataRecebida);
                            
                          }else{
                            String nomeTabela = "table_" + Random().nextInt(10000000).toString();
                            data = new MetaModel(
                              valorInputBoxController.text,
                              motivoInputBoxController.text,
                              nomeTabela,
                              false,
                            );
                            int id = await db.inserirMeta(data);
                            data.id = id.toString();
                          }
                          Navigator.of(context).pop(data); 
                        }
                      },
                      child: Text("Salvar".i18n),
                    ),
                  ],
                )
              ],
            ),
          ),
          Positioned(
            left: Consts.padding,
            right: Consts.padding,
            child: CircleAvatar(
              child: Image.asset('assets/icon.png'),
              backgroundColor: Colors.transparent,
              radius: Consts.avatarRadius,
            ),
          ),
        ],
      ),
    );
  }
}

class Consts {
  Consts._();

  static const double padding = 16.0;
  static const double avatarRadius = 66.0;
}
