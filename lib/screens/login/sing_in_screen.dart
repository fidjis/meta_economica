import 'package:flutter/material.dart';
import 'package:meta_economica/screens/login/google/sign_in_page_google.dart';
import 'package:meta_economica/screens/login/ios/signIn_page_ios.dart';

class SignInPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(child: FlatButton(
            onPressed: (){
              Navigator.push(
               context,
               MaterialPageRoute(builder: (context) => SignInPageIOS()),
              );
            }, 
            child: Text("IOS login"))
          ),
          Expanded(child: FlatButton(
            onPressed: (){
              Navigator.push(
               context,
               MaterialPageRoute(builder: (context) => SignInPageGOOGLE()),
              );
            }, 
            child: Text("GOOGLE login"))
          ),
        ],
      ),
    );
  }
}

