import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:meta_economica/database/database_crud.dart';
import 'package:meta_economica/models/dataModel.dart';
import 'package:meta_economica/models/metaModel.dart';
import 'package:meta_economica/my_widgets/custom_dialog_meta.dart';
import 'package:meta_economica/my_widgets/custom_dialog_meta_completa.dart';
import 'package:meta_economica/my_widgets/custom_dialog_meta_list.dart';
import 'package:meta_economica/my_widgets/data_widget.dart';
import 'package:meta_economica/my_widgets/custom_dialog.dart';
import 'package:meta_economica/localizations/home_screen.i18n.dart';
import 'package:vector_math/vector_math.dart' as math;

import 'config_screen.dart';
import 'privacy_policy_screen.dart';
import 'sobre_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  var primaryColor = const Color(0xFF753EE0);
  var segundaryColor = Colors.white;
  var terciaryColor = const Color(0xFFA581E7);

  MetaModel metaModel;

  DatabaseCRUD db = new DatabaseCRUD();
  List<DataModel> dataInicial = new List<DataModel>();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  int valorParapolpar = 0;
  String motivo = "";
  int valorPolpado = 0;
  double valorPolpadoPorcentagem = 0.0;

  void initState() {
    super.initState();
    setProgressValorPolpado();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: terciaryColor,
      drawer: buildDrawer(),
      body: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                flex: 7,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    buildAppBar(),
                    GestureDetector(
                      onTap: (){
                        if(metaModel != null)
                          showCustumDialogMeta(true);
                        //showDialog(
                        //  barrierDismissible: false,
                        //  context: context,
                        //  builder: (BuildContext context) => CustomDialogMeta(title: "Editando..", valorAtual: "Valor atual:\nR\$$valorParapolpar",),
                      // );
                      },
                      child: Container(
                        padding: EdgeInsets.only(bottom: 1.0),
                        child: Card(
                          elevation: 9,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Text(
                                  'Meta'.i18n + ': R\$$valorParapolpar',
                                  textScaleFactor: 1.3,
                                ),
                                Text(
                                  '$motivo',
                                  textScaleFactor: 0.9,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        width: 50,
                        //padding: EdgeInsets.only(bottom: 10),
                        child: LiquidLinearProgressIndicator(
                          value: valorPolpadoPorcentagem, // Defaults to 0.5.
                          valueColor: AlwaysStoppedAnimation(Colors.green), // Defaults to the current Theme's accentColor.
                          backgroundColor: Colors.white, // Defaults to the current Theme's backgroundColor.
                          //borderColor: Colors.red,
                          //borderWidth: 5.0,
                          borderRadius: 12.0,
                          direction: Axis.vertical, // The direction the liquid moves (Axis.vertical = bottom to top, Axis.horizontal = left to right). Defaults to Axis.horizontal.
                          center: Text("${valorPolpadoPorcentagem.toStringAsFixed(2).replaceFirst('0.', '')}%"),
                        ),
                      ),
                    ),
                    Container(
                      //padding: EdgeInsets.only(bottom: 50.0),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            'Meu saldo'.i18n + ' R\$$valorPolpado',
                            textScaleFactor: 1.1,
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ),
              Expanded(flex: 3, child: Container()),
            ],
          ),
          buildDraggableScrollableSheet(),
        ],
      ),
    );
  }

  Drawer buildDrawer() {
    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            height: 100.0,
            child: Stack(alignment: Alignment.center, children: <Widget>[
                buildBackgroundAppName(),
                buildBackgroundAppNameShadow(),
              ],),
            ),
            decoration: BoxDecoration(
              color: terciaryColor
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 30, bottom: 30, left: 20, right: 20),
            alignment: Alignment.center,
            child: Text(
              "“Se você almeja ser rico, pense em poupar assim como você pensa em ganhar.” – Benjamin Franklin".i18n,
              //textScaleFactor: 0.9,
              textAlign: TextAlign.center,
            ),
          ),
          Divider(),
          ListTile(
            leading: Icon(
              Icons.plus_one,
              color: primaryColor,
            ),
            title: Text('Novo Registro'.i18n),
            onTap: () {
              Navigator.pop(context);
              showDialogCustomNovoRegistro();
            },
          ),
          ListTile(
            leading: Icon(
              Icons.monetization_on,
              color: primaryColor,
            ),
            title: Text("Minhas Metas".i18n),
            onTap: () {
              Navigator.pop(context);
              openMinhasMetasDialog();
            },
          ),
          //buildButtonConfig(),
          ListTile(
            leading: Icon(
              Icons.lock,
              color: primaryColor,
            ),
            title: Text("Politica de Privacidade".i18n),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => PrivacyPolicyScreen()),
              );
            },
          ),
          ListTile(
            leading: Icon(
              Icons.info,
              color: primaryColor,
            ),
            title: Text("Sobre".i18n),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SobreScreen()),
              );
            },
          ),
          Divider(),
        ],
      ),
    );
  }

  ListTile buildButtonConfig() {
    return ListTile(
          leading: Icon(
            Icons.build,
            color: primaryColor,
          ),
          title: Text("Configurações".i18n),
          onTap: () {
            Navigator.pop(context);
            //Navigator.push(
             // context,
             // MaterialPageRoute(builder: (context) => ConfigScreen()),
            //);
            showDialog(
              context: context,
              builder: (BuildContext context) {
                // retorna um objeto do tipo Dialog
                return AlertDialog(
                  title: new Text("Em Construção!".i18n),
                  content: new Text("Aguarde as proximas atualizações.".i18n),
                  actions: <Widget>[
                    new FlatButton(
                      child: new Text("Fechar".i18n),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                );
              },
            );
          },
        );
  }

  Container buildAppBar() {
    return Container(
      margin: EdgeInsets.only(bottom: 10.0),
      height: 160.0,
      decoration: new BoxDecoration(
      color: terciaryColor,
      boxShadow: [
        new BoxShadow(blurRadius: 40.0)
      ],
      borderRadius: new BorderRadius.vertical(
        bottom: new Radius.elliptical(
          MediaQuery.of(context).size.width, 100.0)),
      ),
      child: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width * 0.9,
            height: 100.0,
            child: Stack(alignment: Alignment.center, children: <Widget>[
              buildBackgroundAppName(),
              buildBackgroundAppNameShadow(),
            ],),
          ),
          Positioned(
            top: 80.0,
            left: 0.0,
            right: 0.0,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: DecoratedBox(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(1.0),
                  color: Colors.white
                ),
                child: Row(
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.menu,
                        color: primaryColor,
                      ),
                      onPressed: () {
                        _scaffoldKey.currentState.openDrawer();
                      },
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.center,
                        child: Text(
                          "“Cuidado com as pequenas despesas”".i18n,
                          textScaleFactor: 0.9,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.monetization_on,
                        color: primaryColor,
                      ),
                      onPressed: () {
                        openMinhasMetasDialog();
                      },
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  //Quando receber o event como parametro, ele inicia o processo de edicao
  Future showCustumDialogMeta(bool isEditMode) async {
    MetaModel result = await Navigator.of(context).push(new PageRouteBuilder<MetaModel>(
      opaque: false,
      pageBuilder: (BuildContext context, _, __) {
        if(!isEditMode)
          return new CustomDialogMeta(isEditMode: isEditMode);
        else
          return new CustomDialogMeta(dataRecebida: metaModel, isEditMode: isEditMode, valorAtual: "Valor atual:".i18n + "\nR\$$valorParapolpar",);
      },
      fullscreenDialog: true
    ));
    if (result != null) {
      trocarMeta(result);
    }
  }

  trocarMeta(MetaModel metaSelected){
    if (metaSelected != null) {
      setState(() {
        metaModel = metaSelected; 
        //valorParapolpar = double.parse(result.valor);
        valorParapolpar = int.parse(metaSelected.valor);
        motivo = metaSelected.motivo;
        valorPolpado = 0;
        valorPolpadoPorcentagem = 0.0;
      });
    }
    setProgressValorPolpado();
  }
  
  bool dialogJamostrado = false;
  showDialogMetaCompleta(){
    if(!dialogJamostrado){
      dialogJamostrado = true;
      showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {},
        barrierDismissible: true,
        barrierColor: Colors.black.withOpacity(0.4),
        barrierLabel: '',
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.rotate(
            angle: math.radians(anim1.value * 360),
            child: Opacity(
              opacity: anim1.value,
              child: CustomDialogMetaCompleta(),
            ),
          );
        },
        transitionDuration: Duration(milliseconds: 300)
      );
    }
  }

  Padding buildBackgroundAppName() {
    return Padding(
      //padding: const EdgeInsets.fromLTRB(15.0, 30.0, 15.0, 15.0, ),
      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0, ),
      child: Text(
        "Meta Econômica".i18n,
        style: TextStyle(
          color: primaryColor,
          fontSize: 30,
          fontFamily: "Lobster",
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Padding buildBackgroundAppNameShadow() {
    return Padding(
      //padding: const EdgeInsets.fromLTRB(20.0, 35.0, 20.0, 20.0,),
      padding: const EdgeInsets.fromLTRB(7.0, 7.0, 0.0, 0.0,),
      child: Text(
        "Meta Econômica".i18n,
        style: TextStyle(
          color: Colors.white,
          fontSize: 30,
          fontFamily: "Lobster",
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Container buildDraggableScrollableSheet() {
    return Container(
      child: DraggableScrollableSheet(
        initialChildSize: 0.3,
        minChildSize: 0.3,
        maxChildSize: 0.8,
        builder: (BuildContext context, myscrollController) {
          return Column(
            children: <Widget>[
              buildDragCabecalho(),
              Expanded(child: Container(
                decoration: new BoxDecoration(
                  color: segundaryColor,
                  borderRadius: new BorderRadius.only(
                    topLeft:  const  Radius.circular(40.0),
                    topRight: const  Radius.circular(40.0))
                ),
                child: Padding(
                  padding: const EdgeInsets.only(top: 30.0),
                  child: FutureBuilder<List<DataModel>>(
                    future: db.consultar(metaModel?.nomeTabela),
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      setProgressValorPolpado();
                      if(snapshot.hasError)
                        return Center(
                          child: Text("Ocorreu um erro ao carregar os dados".i18n),
                        );
                      else if (!snapshot.hasData)
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      else if(snapshot.data.length == 0)
                        return Center(
                          child: Text(
                            'Adicione seus dados!'.i18n,
                            style: TextStyle(
                                fontSize: 16.0,
                            ),
                          ),
                        );
                      else{
                        return ListView.builder(
                          controller: myscrollController,
                          itemCount: snapshot.data.length,
                          itemBuilder: (context, index) {
                            var a = DataWidget(
                              data: DataModel(
                                "R\$${snapshot.data[snapshot.data.length - 1 -index].valor}",
                                "${snapshot.data[snapshot.data.length - 1 -index].data}",
                                snapshot.data[snapshot.data.length - 1 -index].isSaldo,
                                "${snapshot.data[snapshot.data.length - 1 -index].comentario}"
                              ),
                            );
                            return DataWidget(
                              data: DataModel(
                                "R\$${snapshot.data[index].valor}",
                                "${snapshot.data[index].data}",
                                snapshot.data[index].isSaldo,
                                "${snapshot.data[index].comentario}"
                              ),
                            );
                          },
                        );
                      }
                    },   
                  )
                ),
              )),
            ],
          );
        },
      ),
    );
  }

  setProgressValorPolpado() async {
    List<MetaModel> metas = await db.consultarMetas();
    if(metas.isNotEmpty && metaModel == null){
      for (var i = 0; i < metas.length -1; i++) {
        if(!metas[i].isCompleted){
          metaModel = metas[i];
          break;
        }
      }
      valorParapolpar = int.parse(metaModel.valor);
      motivo = metaModel.motivo;
    }

    if(metaModel != null)
      dataInicial = await db.consultar(metaModel.nomeTabela);
      

    valorPolpado = 0;
    if(dataInicial != null && metaModel != null){
      dataInicial.forEach((data){
        if(data.isSaldo)
          setState(() {
            //valorPolpado += double.parse(data.valor.replaceFirst(',', '.'));
            valorPolpado += int.parse(data.valor.replaceFirst(',', '.'));
          });
        else
          setState(() {
            //valorPolpado -= double.parse(data.valor.replaceFirst(',', '.'));
            valorPolpado -= int.parse(data.valor.replaceFirst(',', '.'));
          });
      });

      //calcular porcentagem atual do
      setState(() {
        valorPolpadoPorcentagem = (((valorParapolpar - valorPolpado) / valorParapolpar) - 1); // * (-1);
        if(valorPolpadoPorcentagem < 0)
          valorPolpadoPorcentagem = valorPolpadoPorcentagem * (-1);
      });

      if(valorPolpadoPorcentagem >= 1.0)
        Future.delayed(const Duration(milliseconds: 600), () {
          showDialogMetaCompleta();
          atualizarIscompletedAcimaDe1();
        });
      else
        Future.delayed(const Duration(milliseconds: 600), () {
          dialogJamostrado = false;
          atualizarIscompletedAbaixoDe1();
        });
    }
  }

  bool jaAtualizadoAcimaDe1 = false;
  bool jaAtualizadoAbaixoDe1 = false;
  atualizarIscompletedAcimaDe1(){
    if(!jaAtualizadoAcimaDe1){
      jaAtualizadoAcimaDe1 = true;
      jaAtualizadoAbaixoDe1 = false;
      metaModel.isCompleted = true;
      db.atualizarMetas(metaModel);
    }
  }
  atualizarIscompletedAbaixoDe1(){
    if(!jaAtualizadoAbaixoDe1){
      jaAtualizadoAcimaDe1 = false;
      jaAtualizadoAbaixoDe1 = true;
      metaModel.isCompleted = false;
      db.atualizarMetas(metaModel);
    }
  }

  buildDragCabecalho() {
    return Container(
      margin: EdgeInsets.only(bottom: 3.0),
      //color: terciaryColor,
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(bottom: 5.0),
            height: 60.0,
            width: MediaQuery.of(context).size.width * 0.85,
            decoration: new BoxDecoration(
              color: terciaryColor,
              boxShadow: [
                new BoxShadow(blurRadius: 40.0)
              ],
              borderRadius: new BorderRadius.vertical(
                top: new Radius.elliptical(
                  MediaQuery.of(context).size.width, 140.0)),
            ),
            alignment: Alignment.bottomCenter,
            child:  FloatingActionButton.extended(
              onPressed: (){
                showDialogCustomNovoRegistro();
              },
              heroTag: "Novo Registro",
              backgroundColor: primaryColor,
              icon: Icon(Icons.add_circle_outline),
              label: Text("Novo Registro".i18n)
            ),
          ),
        ],
      ),
    );
  }

  void showDialogCustomNovoRegistro() {
    if(metaModel != null)
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => CustomDialog(metaModel: metaModel,),
      );
    else
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          // retorna um objeto do tipo Dialog
          return AlertDialog(
            title: new Text("Atenção!".i18n),
            content: new Text("Antes de registrar adicione uma meta"),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Adicionar".i18n),
                onPressed: () {
                  Navigator.of(context).pop();
                  showCustumDialogMeta(false);
                },
              ),
            ],
          );
        },
      );
  }

  void openMinhasMetasDialog() {
    bool temp = true;
    if(temp)
      showDialog(
        //barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => CustomDialogMetaList(
          callback: () {showCustumDialogMeta(false);},
          callbackMetaSelected: (metaSelected) {trocarMeta(metaSelected);},
        ),
      );
    else
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // retorna um objeto do tipo Dialog
          return AlertDialog(
            title: new Text("Em Construção!".i18n),
            content: new Text("Aguarde as proximas atualizações.".i18n),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Fechar".i18n),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
  }
}