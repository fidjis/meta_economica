import 'package:flutter/material.dart';
import 'package:meta_economica/localizations/privacy_policy_screen.i18n.dart';

class PrivacyPolicyScreen extends StatefulWidget {

  @override
  _PrivacyPolicyScreenState createState() => _PrivacyPolicyScreenState();
}

class _PrivacyPolicyScreenState extends State<PrivacyPolicyScreen> {
  var primaryColor = const Color(0xFF753EE0);
  var segundaryColor = Colors.white;
  var terciaryColor = const Color(0xFFA581E7);

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: terciaryColor,
      body: Column(
        mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Center(child: buildAppBar()),
              Expanded(
                child: SingleChildScrollView(
                  child: Container (
                      padding: const EdgeInsets.all(16.0),
                      width: MediaQuery.of(context).size.width*0.95,
                      child: Column (
                        mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            new Text ("Fidjis built the Meta Economica app as a Freemium app. This SERVICE is provided by Fidjis at no cost and is intended for use as is.", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            new Text ("This page is used to inform visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            new Text ("If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. we will not use or share your information with anyone except as described in this Privacy Policy.", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            new Text ("The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at [App name] unless otherwise defined in this Privacy Policy.", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("**Information Collection and Use**", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            new Text ("For a better experience, while using our Service, we may require you to provide us with certain personally identifiable information, including but not limited to Meta Econômica. The information that we request will be retained by us and used as described in this privacy policy.", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            new Text ("The app does use third party services that may collect information used to identify you.", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            new Text ("Link to privacy policy of third party service providers used by the app", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("* [Google Play Services] (https://www.google.com/policies/privacy/)", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            new Text ("* [AdMob] (https://support.google.com/admob/answer/6128543?hl=en)", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            new Text ("* [Google Analytics for Firebase] (https://firebase.google.com/policies/analytics)", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("**Log Data**", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("we want to inform you that whenever you use our Service, in a case of an error in the app we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing our Service, the time and date of your use of the Service, and other statistics.", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("**Cookies**", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device's internal memory.", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            new Text ("This Service does not use these “cookies” explicitly. However, the app may use third party code and libraries that use “cookies” to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("**Service Providers**", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("we may employ third-party companies and individuals due to the following reasons:", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("* To facilitate our Service;", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            new Text ("* To provide the Service on our behalf;", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            new Text ("* To perform Service-related services; or", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            new Text ("* To assist us in analyzing how our Service is used.", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("we want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("**Security**", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("we value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("**Links to Other Sites**", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. we have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("**Children’s Privacy**", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("These Services do not address anyone under the age of 13. we do not knowingly collect personally identifiable information from children under 13\. In the case we discover that a child under 13 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("**Changes to This Privacy Policy**", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("we may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. we will notify you of any changes by posting the new Privacy Policy on this page.", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("This policy is effective as of 2020-05-13", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("**Contact Us**", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 20,),
                            new Text ("If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at eversondias2011@outlook.com.", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 40,),
                            new Text ("This privacy policy page was created at [privacypolicytemplate.net](https://privacypolicytemplate.net) and modified/generated by [App Privacy Policy Generator](https://app-privacy-policy-generator.firebaseapp.com/)", textAlign: TextAlign.justify, style: TextStyle(color: segundaryColor),),
                            SizedBox(height: 50,)
                          ],
                        ),
                      ),
                ),
              ),
            ],
          ),
    );
  }

  Container buildAppBar() {
    return Container(
      margin: EdgeInsets.only(bottom: 10.0),
      height: 160.0,
      decoration: new BoxDecoration(
      color: terciaryColor,
      boxShadow: [
        new BoxShadow(blurRadius: 40.0)
      ],
      borderRadius: new BorderRadius.vertical(
        bottom: new Radius.elliptical(
          MediaQuery.of(context).size.width, 100.0)),
      ),
      child: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width * 0.9,
            height: 100.0,
            child: Stack(alignment: Alignment.center, children: <Widget>[
              buildBackgroundAppName(),
              buildBackgroundAppNameShadow(),
            ],),
          ),
          Positioned(
            top: 80.0,
            left: 0.0,
            right: 0.0,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: DecoratedBox(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(1.0),
                  color: Colors.white
                ),
                child: Row(
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                        color: primaryColor,
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.center,
                        child: Text(
                          "Politica de Privacidade".i18n,
                          textScaleFactor: 1.2,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.lock,
                        color: primaryColor,
                      ),
                      onPressed: () {
                        
                      },
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Padding buildBackgroundAppName() {
    return Padding(
      //padding: const EdgeInsets.fromLTRB(15.0, 30.0, 15.0, 15.0, ),
      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0, ),
      child: Text(
        "Meta Econômica".i18n,
        style: TextStyle(
          color: primaryColor,
          fontSize: 30,
          fontFamily: "Lobster",
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Padding buildBackgroundAppNameShadow() {
    return Padding(
      //padding: const EdgeInsets.fromLTRB(20.0, 35.0, 20.0, 20.0,),
      padding: const EdgeInsets.fromLTRB(7.0, 7.0, 0.0, 0.0,),
      child: Text(
        "Meta Econômica".i18n,
        style: TextStyle(
          color: Colors.white,
          fontSize: 30,
          fontFamily: "Lobster",
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

 
}