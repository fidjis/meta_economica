import 'package:flutter/material.dart';
import 'package:meta_economica/database/database_crud.dart';
import 'package:meta_economica/in_app/in_app_purcheses.dart';
import 'package:meta_economica/models/metaModel.dart';
import 'package:meta_economica/localizations/custom_dialog_meta_list.i18n.dart';

import 'meta_widget.dart';

class CustomDialogMetaList extends StatefulWidget {

  CustomDialogMetaList({
    this.callback,
    @required this.callbackMetaSelected,
  });

  Function callback; //vai chamar o dialog para add nova meta //showCustumDialogMeta
  Function(MetaModel metaSelected) callbackMetaSelected; 

  @override
  _CustomDialogMetaListState createState() => _CustomDialogMetaListState();
}

class _CustomDialogMetaListState extends State<CustomDialogMetaList> {
  
  var primaryColor = const Color(0xFF753EE0);
  var segundaryColor = Colors.white;
  var terciaryColor = const Color(0xFFA581E7);

  DatabaseCRUD db = new DatabaseCRUD();

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Consts.padding),
      ),      
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                  top: Consts.avatarRadius + Consts.padding + 40,
                  bottom: Consts.padding,
                  left: Consts.padding,
                  right: Consts.padding,
                ),
                margin: EdgeInsets.only(top: Consts.avatarRadius),
                decoration: new BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(Consts.padding),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      blurRadius: 10.0,
                      offset: const Offset(0.0, 10.0),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min, // To make the card compact
                  children: <Widget>[
                    Divider(),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.4,
                      //padding: const EdgeInsets.only(top: 0.0, bottom: 30.0),
                      child: FutureBuilder<List<MetaModel>>(
                        future: db.consultarMetas(),
                        builder: (BuildContext context, AsyncSnapshot snapshot) {
                          if(snapshot.hasError)
                            return Center(
                              child: Text("Ocorreu um erro ao carregar os dados".i18n),
                            );
                          else if (!snapshot.hasData)
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          else if(snapshot.data.length == 0)
                            return Center(
                              //padding: const EdgeInsets.only(top: 80),
                              child: Text(
                                'Adicione suas Metas!'.i18n,
                                style: TextStyle(
                                    fontSize: 16.0,
                                ),
                              ),
                            );
                          else{
                            return ListView.builder(
                              //controller: myscrollController,
                              itemCount: snapshot.data.length,
                              shrinkWrap: true,
                              //physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) {
                                return GestureDetector(
                                  onTap: (){
                                    Navigator.of(context).pop();
                                    widget.callbackMetaSelected(snapshot.data[snapshot.data.length - 1 -index]);
                                  },
                                  child: MetaWidget(
                                    data: snapshot.data[snapshot.data.length - 1 -index],
                                  ),
                                );
                              },
                            );
                          }
                        },   
                      )
                    ),
                    Divider()
                  ],
                ),
              ),
              Positioned(
                left: Consts.padding,
                right: Consts.padding,
                child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        CircleAvatar(
                          backgroundColor: terciaryColor,
                          radius: Consts.avatarRadius,
                        ),
                        FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => MarketScreen()),
                            );
                          },
                          child: Text("QUERO SER PRO!", textAlign: TextAlign.center, style: TextStyle(color: Colors.red))
                        ),
                      ],
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text("Metas".i18n, textScaleFactor: 1.9, style: TextStyle(color: segundaryColor),),
                        Text("Free: Limite de 1".i18n, textScaleFactor: 0.7, style: TextStyle(color: segundaryColor),),
                        SizedBox(height: 50,),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 5,),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FloatingActionButton.extended(
                backgroundColor: terciaryColor,
                onPressed: () {
                  Navigator.of(context).pop();
                  widget.callback(); //chamando o dialog que adiciona nova meta
                },
                label: Text("Nova Meta", textAlign: TextAlign.center, style: TextStyle(color: segundaryColor))
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class Consts {
  Consts._();

  static const double padding = 16.0;
  static const double avatarRadius = 66.0;
}
