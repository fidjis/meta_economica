import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

    static var _t = Translations("pt_br") +
        {
          "pt_br": "Ocorreu um erro ao carregar os dados",
          "en_us": "There was an error loading data",
        } +
        {
          "pt_br": "Adicione suas Metas!",
          "en_us": "Add your Goals!",
        } +
        {
          "pt_br": "Metas",
          "en_us": "Goals!",
        } +
        {
          "pt_br": "Free: Limite de 1",
          "en_us": "Free: Limit 1",
        };

  String get i18n => localize(this, _t);
}

