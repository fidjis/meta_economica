import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:meta_economica/database/database_crud.dart';
import 'package:meta_economica/models/dataModel.dart';
import 'package:meta_economica/models/metaModel.dart';
import 'package:meta_economica/localizations/custom_dialog.i18n.dart';

class CustomDialog extends StatefulWidget {

  final MetaModel metaModel; 

  CustomDialog({
    @required this.metaModel,
  });

  @override
  _CustomDialogState createState() => _CustomDialogState();
}

class _CustomDialogState extends State<CustomDialog> {

  var primaryColor = const Color(0xFF753EE0);
  var segundaryColor = Colors.white;
  var terciaryColor = const Color(0xFFA581E7);
  
  DatabaseCRUD db = new DatabaseCRUD();
  DateTime selectedDate = DateTime.now();
  final valorInputBoxController = TextEditingController();
  final comentarioInputBoxController = TextEditingController();
  List<bool> isSelected = [true, false]; //so precisa salvar "isSelected[0]" primeiro indice

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Consts.padding),
      ),      
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  salvar() async {
    if(valorInputBoxController.text.isEmpty)
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          // retorna um objeto do tipo Dialog
          return AlertDialog(
            //title: new Text("Atenção!"),
            content: Center(child: new Text("Preencha todos os campos!".i18n)),
            actions: <Widget>[
              new FlatButton(
                child: new Text("ok"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    else{
      DataModel dataModel = DataModel(
        valorInputBoxController.text, 
        selectedDate.day.toString() + "/" + selectedDate.month.toString() + "/" + selectedDate.year.toString(), 
        isSelected[0], 
        comentarioInputBoxController.text
      );
      db.inserir(dataModel, widget.metaModel.nomeTabela);
    }
  }

  dialogContent(BuildContext context) {
    return SingleChildScrollView(
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: Consts.avatarRadius + Consts.padding,
              bottom: Consts.padding,
              left: Consts.padding,
              right: Consts.padding,
            ),
            margin: EdgeInsets.only(top: Consts.avatarRadius),
            decoration: new BoxDecoration(
              color: Colors.white,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(Consts.padding),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 10.0,
                  offset: const Offset(0.0, 10.0),
                ),
              ],
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min, // To make the card compact
              children: <Widget>[
                Text(
                  "Resgistro".i18n,
                  style: TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  controller: valorInputBoxController,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    WhitelistingTextInputFormatter.digitsOnly
                  ],
                  decoration: new InputDecoration(
                    hintText: "Valor".i18n + " R\$",
                  ),
                ),
                SizedBox(height: 16.0),
                buildToggleButtons(),
                SizedBox(height: 14.0),
                TextFormField(
                  controller: comentarioInputBoxController,
                  decoration: new InputDecoration(
                    hintText: "Comentario".i18n,
                  ),
                ),
                SizedBox(height: 24.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop(); // To close the dialog
                      },
                      child: Text("Cancelar".i18n, style: TextStyle(color: Colors.red)),
                    ),
                    FlatButton(
                      onPressed: () {
                        salvar();
                        Navigator.of(context).pop(); // To close the dialog
                      },
                      child: Text("Salvar".i18n),
                    ),
                  ],
                )
              ],
            ),
          ),
          Positioned(
            left: Consts.padding,
            right: Consts.padding,
            child: CircleAvatar(
              child: Image.asset('assets/icon.png'),
              backgroundColor: Colors.transparent,
              radius: Consts.avatarRadius,
            ),
          ),
        ],
      ),
    );
  }

  buildToggleButtons(){
    return ToggleButtons(
      borderColor: Colors.black,
      fillColor: primaryColor,
      borderWidth: 2,
      selectedBorderColor: Colors.black,
      selectedColor: segundaryColor,
      borderRadius: BorderRadius.circular(0),
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Text(
            '    É um Saldo    '.i18n,
            style: TextStyle(fontSize: 16),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Text(
            'É uma Retirada'.i18n,
            style: TextStyle(fontSize: 16),
          ),
        ),
      ],
      onPressed: (int index) {
        setState(() {
          for (int i = 0; i < isSelected.length; i++) {
            if (i == index) { 
              isSelected[i] = true; 
            } else {
              isSelected[i] = false; 
            }
          }
        });
      },
      isSelected: isSelected,
    );
  }
}

class Consts {
  Consts._();

  static const double padding = 16.0;
  static const double avatarRadius = 66.0;
}
