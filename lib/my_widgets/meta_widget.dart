import 'package:flutter/material.dart';
import 'package:meta_economica/models/metaModel.dart';

class MetaWidget extends StatefulWidget {

  MetaWidget({
    @required this.data,
  });

  final MetaModel data;

  @override
  _MetaWidgetState createState() => _MetaWidgetState();
}

class _MetaWidgetState extends State<MetaWidget> {

  var primaryColor = const Color(0xFF753EE0);
  var segundaryColor = Colors.white;
  var terciaryColor = const Color(0xFFA581E7);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if(widget.data.isCompleted)
      return buildIndividualCardMetaCompleted();
    else
      return buildIndividualCard();
  }

  buildIndividualCardMetaCompleted() {
    return Row(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Visibility(
          //visible: leftVisibility,
          visible: true,
          child: Container(
            margin: EdgeInsets.only(right: 5.0, left: 5.0),
            decoration: BoxDecoration(
              color: primaryColor,
              borderRadius: new BorderRadius.circular(3.0)
            ),
            width: 5.0,
            height: 60,
          ),
        ),
        Container(
          child: Icon(Icons.star, color: Colors.yellow[700], size: 35.0,),
        ),
        Expanded(
          child: Card(
            margin: EdgeInsets.only(bottom: 5.0),
            //color: Colors.white70,
            color: Colors.yellow[200],
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.fromLTRB(45.0, 10.0, 45.0, 10.0),
                  height: 85.0,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "${widget.data.motivo}",
                            style: TextStyle(
                              fontSize: 16,
                              //color: primaryColor
                            ),
                          ),
                        ],
                      ),
                      Divider(),
                      Text(
                        "${widget.data.valor} - ALCANÇADO",
                        style: TextStyle(
                          fontSize: 10,
                          //color: Colors.white
                        ),
                      ),
                    ],
                  )
                ),
              ],
            ),
          ),
        ),
        Container(
          child: Icon(Icons.star, color: Colors.yellow[700], size: 35.0,),
        ),
        Visibility(
          //visible: rightVisibility,
          visible: true,
          child: Container(
            margin: EdgeInsets.only(right: 5.0, left: 5.0),
            decoration: BoxDecoration(
              color: primaryColor,
              borderRadius: new BorderRadius.circular(3.0)
            ),
            width: 5.0,
            height: 60,
          ),
        ),
      ],
    );
  }

  buildIndividualCard() {
    return Row(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Visibility(
          //visible: leftVisibility,
          visible: true,
          child: Container(
            margin: EdgeInsets.only(right: 5.0, left: 5.0),
            decoration: BoxDecoration(
              color: primaryColor,
              borderRadius: new BorderRadius.circular(3.0)
            ),
            width: 5.0,
            height: 60,
          ),
        ),
        Expanded(
          child: Card(
            margin: EdgeInsets.only(bottom: 5.0),
            color: Colors.white70,
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.fromLTRB(45.0, 10.0, 45.0, 10.0),
                  height: 85.0,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "${widget.data.motivo}",
                            style: TextStyle(
                              fontSize: 16,
                              //color: primaryColor
                            ),
                          ),
                        ],
                      ),
                      Divider(),
                      Text(
                        "${widget.data.valor}",
                        style: TextStyle(
                          fontSize: 10,
                          //color: Colors.white
                        ),
                      ),
                    ],
                  )
                ),
              ],
            ),
          ),
        ),
        Visibility(
          //visible: rightVisibility,
          visible: true,
          child: Container(
            margin: EdgeInsets.only(right: 5.0, left: 5.0),
            decoration: BoxDecoration(
              color: primaryColor,
              borderRadius: new BorderRadius.circular(3.0)
            ),
            width: 5.0,
            height: 60,
          ),
        ),
      ],
    );
  }
}