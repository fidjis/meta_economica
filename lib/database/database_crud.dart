import 'package:meta_economica/database/database_helper.dart';
import 'package:meta_economica/models/dataModel.dart';
import 'package:meta_economica/models/metaModel.dart';

class DatabaseCRUD {
  // referencia nossa classe single para gerenciar o banco de dados
  final dbHelper = DatabaseHelper.instance;
  
  void inserir(DataModel values, String table) async {
    // linha para incluir
    Map<String, dynamic> row = {
      DatabaseHelper.columnId : values.id,
      DatabaseHelper.columnValor : values.valor,
      DatabaseHelper.columnData  : values.data,
      DatabaseHelper.columnComentario : values.comentario,
      DatabaseHelper.columnIsSaldo : values.isSaldo
    };
    final id = await dbHelper.insert(row, table);
    print('linha inserida id: $id');
  }

  Future _createTable(String table) async {
    await dbHelper.createTable(table);
  }  

  Future<int> inserirMeta(MetaModel values) async {
    // linha para incluir
    Map<String, dynamic> row = {
      DatabaseHelper.columnId : values.id,
      DatabaseHelper.columnValor : values.valor,
      DatabaseHelper.columnMotivo  : values.motivo,
      DatabaseHelper.columnNomeTabela : values.nomeTabela,
      DatabaseHelper.columnIscompleted : values.isCompleted
    };
    final id = await dbHelper.insert(row, DatabaseHelper.tableMetas);
    await _createTable(values.nomeTabela);
    print('linha inserida id: $id');
    return id;
  }

  Future<List<MetaModel>> consultarMetas() async {
    final todasLinhas = await dbHelper.queryAllRows(DatabaseHelper.tableMetas);
    //todasLinhas.forEach((row) => print('$row\n'));
    List<MetaModel> events = new List<MetaModel>();
    todasLinhas.forEach((row){
      Map<String, dynamic> rowMap = row;
      MetaModel m = new MetaModel(
        //eventID: rowMap[DatabaseHelper.columnId].toString(),
        rowMap[DatabaseHelper.columnValor],
        rowMap[DatabaseHelper.columnMotivo],
        rowMap[DatabaseHelper.columnNomeTabela],
        rowMap[DatabaseHelper.columnIscompleted].toString() == '1',
      );
      m.id = rowMap[DatabaseHelper.columnId].toString();
      events.add(m);
    });
    
    return events;
  }

  Future<List<DataModel>> consultar( String table) async {
    final todasLinhas = await dbHelper.queryAllRows( table);
    //todasLinhas?.forEach((row) => print('$row\n'));
    List<DataModel> events = new List<DataModel>();
    todasLinhas?.forEach((row){
      Map<String, dynamic> rowMap = row;
      events.add(new DataModel(
        //eventID: rowMap[DatabaseHelper.columnId].toString(),
        rowMap[DatabaseHelper.columnValor],
        rowMap[DatabaseHelper.columnData],
        rowMap[DatabaseHelper.columnIsSaldo].toString() == '1',
        rowMap[DatabaseHelper.columnComentario],
      ));
    });
    
    return events;
  }
  
  void atualizarMetas(MetaModel event) async {
    // linha para atualizar
    Map<String, dynamic> row = {
      DatabaseHelper.columnId : event.id.toString(),
      DatabaseHelper.columnValor : event.valor,
      DatabaseHelper.columnMotivo  : event.motivo,
      DatabaseHelper.columnNomeTabela : event.nomeTabela,
      DatabaseHelper.columnIscompleted : event.isCompleted
    };
    final linhasAfetadas = await dbHelper.update(row, DatabaseHelper.tableMetas);
    print('atualizadas $linhasAfetadas linha(s)');
  }
  
  void deletar(int row, String table) async {
    // Assumindo que o numero de linhas é o id para a última linha
    //final id = await dbHelper.queryRowCount();
    final linhaDeletada = await dbHelper.delete(row, table);
    print('Deletada(s) $linhaDeletada linha(s): linha $row');
  }
  
  void deletarTudo(String table) async {
    final todasLinhas = await dbHelper.queryAllRows(table);
    todasLinhas.forEach((row) async {
      Map<String, dynamic> rowMap = row;
      int id = rowMap[DatabaseHelper.columnId];
      await dbHelper.delete(id, table);
    });
    // Assumindo que o numero de linhas é o id para a última linha
    //final id = await dbHelper.queryRowCount();
    
  }
}