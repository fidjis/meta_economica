import 'package:flutter/material.dart';
import 'package:meta_economica/models/dataModel.dart';

class DataWidget extends StatefulWidget {

  DataWidget({
    @required this.data, 
  });

  final DataModel data;

  @override
  _DataWidgetState createState() => _DataWidgetState();
}

class _DataWidgetState extends State<DataWidget> {

  var primaryColor = const Color(0xFF753EE0);
  var segundaryColor = Colors.white;
  var terciaryColor = const Color(0xFFA581E7);

  bool leftVisibility;
  bool rightVisibility;

  @override
  void initState() {
    super.initState();
    if(widget.data.isSaldo){
      leftVisibility = true;
      rightVisibility = false;
    }else if(!(widget.data.isSaldo)){
      leftVisibility = false;
      rightVisibility = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        buildIndividualCard(),
      ],
    );
  }

  buildIndividualCard() {
    return GestureDetector(
      onTap: (){
        //Navigator.push(
          //context,
         // MaterialPageRoute(
         //   builder: (context) => ViewDataScreen(
         //     widget.data
        //    )));
      },
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Visibility(
            //visible: leftVisibility,
            visible: true,
            child: Container(
              margin: EdgeInsets.only(right: 5.0, left: 5.0),
              decoration: BoxDecoration(
                color: primaryColor,
                borderRadius: new BorderRadius.circular(3.0)
              ),
              width: 5.0,
              height: 60,
            ),
          ),
          Expanded(
            child: Card(
              margin: EdgeInsets.only(bottom: 5.0),
              color: Colors.white70,
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(45.0, 10.0, 45.0, 10.0),
                    height: 85.0,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "${widget.data.valor}",
                              style: TextStyle(
                                fontSize: 16,
                                //color: primaryColor
                              ),
                            ),
                            Text(
                              "${widget.data.data}",
                              style: TextStyle(
                                fontSize: 16,
                                //color: primaryColor
                              ),
                            )
                          ],
                        ),
                        Divider(),
                        Text(
                          "${widget.data.comentario}",
                          style: TextStyle(
                            fontSize: 10,
                            //color: Colors.white
                          ),
                        ),
                      ],
                    )
                  ),
                  Visibility(
                    visible: leftVisibility,
                    child: Positioned(
                      left: 0.0,
                      child: Container(
                        height: 40,
                        width: 40,
                        decoration: new BoxDecoration(
                          color: Colors.green,
                          borderRadius: new BorderRadius.horizontal(
                            right: new Radius.elliptical(100.0, 100.0)),
                        ),
                        child: Icon(Icons.arrow_upward, color: Colors.green[900],),
                      )
                    ),
                  ),
                  Visibility(
                    visible: rightVisibility,
                    child: Positioned(
                      right: 0.0,
                      child: Container(
                        height: 40,
                        width: 40,
                        decoration: new BoxDecoration(
                          color: Colors.red,
                          borderRadius: new BorderRadius.horizontal(
                            left: new Radius.elliptical(100.0, 100.0)),
                        ),
                        child: Icon(Icons.arrow_downward, color: Colors.red[900],),
                      )
                    ),
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            //visible: rightVisibility,
            visible: true,
            child: Container(
              margin: EdgeInsets.only(right: 5.0, left: 5.0),
              decoration: BoxDecoration(
                color: primaryColor,
                borderRadius: new BorderRadius.circular(3.0)
              ),
              width: 5.0,
              height: 60,
            ),
          ),
        ],
      ),
    );
  }
}