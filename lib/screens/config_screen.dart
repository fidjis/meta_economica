import 'package:flutter/material.dart';
import 'package:meta_economica/localizations/confg_screen.i18n.dart';

class ConfigScreen extends StatefulWidget {

  @override
  _ConfigScreenState createState() => _ConfigScreenState();
}

class _ConfigScreenState extends State<ConfigScreen> {
  
  var primaryColor = const Color(0xFF753EE0);
  var segundaryColor = Colors.white;
  var terciaryColor = const Color(0xFFA581E7);

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: terciaryColor,
      body: Column(
        mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Center(child: buildAppBar()),
              Expanded(
                child: SingleChildScrollView(
                  child: Container (
                    padding: const EdgeInsets.all(16.0),
                    width: MediaQuery.of(context).size.width*0.95,
                    child: Container(padding: EdgeInsets.all(50), alignment: Alignment.center, child: Text("Em Construção!")),
                  ),
                ),
              ),
            ],
          ),
    );
  }

  Container buildAppBar() {
    return Container(
      margin: EdgeInsets.only(bottom: 10.0),
      height: 160.0,
      decoration: new BoxDecoration(
      color: terciaryColor,
      boxShadow: [
        new BoxShadow(blurRadius: 40.0)
      ],
      borderRadius: new BorderRadius.vertical(
        bottom: new Radius.elliptical(
          MediaQuery.of(context).size.width, 100.0)),
      ),
      child: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width * 0.9,
            height: 100.0,
            child: Stack(alignment: Alignment.center, children: <Widget>[
              buildBackgroundAppName(),
              buildBackgroundAppNameShadow(),
            ],),
          ),
          Positioned(
            top: 80.0,
            left: 0.0,
            right: 0.0,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: DecoratedBox(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(1.0),
                  color: Colors.white
                ),
                child: Row(
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                        color: Colors.red,
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.center,
                        child: Text(
                          "Configurações".i18n,
                          textScaleFactor: 1.2,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.build,
                        color: Colors.red,
                      ),
                      onPressed: () {
                        
                      },
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Padding buildBackgroundAppName() {
    return Padding(
      //padding: const EdgeInsets.fromLTRB(15.0, 30.0, 15.0, 15.0, ),
      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0, ),
      child: Text(
        "Meta Econômica".i18n,
        style: TextStyle(
          color: primaryColor,
          fontSize: 30,
          fontFamily: "Lobster",
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Padding buildBackgroundAppNameShadow() {
    return Padding(
      //padding: const EdgeInsets.fromLTRB(20.0, 35.0, 20.0, 20.0,),
      padding: const EdgeInsets.fromLTRB(7.0, 7.0, 0.0, 0.0,),
      child: Text(
        "Meta Econômica".i18n,
        style: TextStyle(
          color: Colors.white,
          fontSize: 30,
          fontFamily: "Lobster",
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}