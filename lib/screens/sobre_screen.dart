import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:meta_economica/localizations/sobre_screen.i18n.dart';

//import 'dart:html' as html;

class SobreScreen extends StatefulWidget {

  @override
  _SobreScreenState createState() => _SobreScreenState();
}

class _SobreScreenState extends State<SobreScreen> {
  var primaryColor = const Color(0xFF753EE0);
  var segundaryColor = Colors.white;
  var terciaryColor = const Color(0xFFA581E7);

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: terciaryColor,
      body: Column(
        mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Center(child: buildAppBar()),
              Expanded(
                child: SingleChildScrollView(
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Developed by:',
                            textScaleFactor: 1.1,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          CircleAvatar(
                            radius: 100,
                            backgroundImage: Image.asset(Assets.avatar).image,
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            'Everson D. Ferreira',
                            textScaleFactor: 2,
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                              'Flutter Lover. Developer. Music.\nLikes Traveling.',
                              style: Theme.of(context).textTheme.caption,
                              textScaleFactor: 1.2,
                              textAlign: TextAlign.center,
                            ),
                          SizedBox(
                            height: 40,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              FlatButton.icon(
                                icon: SizedBox(
                                    width: 20,
                                    height: 20,
                                    child: Image.asset(Assets.github)),
                                label: Text('Github'),
                                onPressed:() async {
                                  if (await canLaunch(Constants.PROFILE_GITHUB)) {
                                    await launch(Constants.PROFILE_GITHUB);
                                  } else {
                                    throw 'Could not launch PROFILE_GITHUB';
                                  }
                                },
                              ),
                              FlatButton.icon(
                                icon: SizedBox(
                                    width: 20,
                                    height: 20,
                                    child: Image.asset(Assets.bitbucket)),
                                label: Text('Bitbucket'),
                                onPressed:() async {
                                  if (await canLaunch(Constants.PROFILE_BITBUCKET)) {
                                    await launch(Constants.PROFILE_BITBUCKET);
                                  } else {
                                    throw 'Could not launch PROFILE_GITHUB';
                                  }
                                },
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              FlatButton.icon(
                                icon: SizedBox(
                                    width: 20,
                                    height: 20,
                                    child: Image.asset(Assets.instagram)),
                                label: Text('Instagram'),
                                onPressed:() async {
                                  if (await canLaunch(Constants.PROFILE_INSTAGRAM)) {
                                    await launch(Constants.PROFILE_INSTAGRAM);
                                  } else {
                                    throw 'Could not launch PROFILE_INSTAGRAM';
                                  }
                                },
                              ),
                              FlatButton.icon(
                                icon: SizedBox(
                                    width: 20,
                                    height: 20,
                                    child: Image.asset(Assets.linkedin)),
                                label: Text('Linkedin'),
                                onPressed:() async {
                                  if (await canLaunch(Constants.PROFILE_LINKEDIN)) {
                                    await launch(Constants.PROFILE_LINKEDIN);
                                  } else {
                                    throw 'Could not launch PROFILE_LINKEDIN';
                                  }
                                },
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
    );
  }

  Container buildAppBar() {
    return Container(
      margin: EdgeInsets.only(bottom: 10.0),
      height: 160.0,
      decoration: new BoxDecoration(
      color: terciaryColor,
      boxShadow: [
        new BoxShadow(blurRadius: 40.0)
      ],
      borderRadius: new BorderRadius.vertical(
        bottom: new Radius.elliptical(
          MediaQuery.of(context).size.width, 100.0)),
      ),
      child: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width * 0.9,
            height: 100.0,
            child: Stack(alignment: Alignment.center, children: <Widget>[
              buildBackgroundAppName(),
              buildBackgroundAppNameShadow(),
            ],),
          ),
          Positioned(
            top: 80.0,
            left: 0.0,
            right: 0.0,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: DecoratedBox(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(1.0),
                  color: Colors.white
                ),
                child: Row(
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                        color: primaryColor,
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.center,
                        child: Text(
                          "Sobre".i18n,
                          textScaleFactor: 1.2,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.info,
                        color: primaryColor,
                      ),
                      onPressed: () {
                        print("your menu action here");
                      },
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Padding buildBackgroundAppName() {
    return Padding(
      //padding: const EdgeInsets.fromLTRB(15.0, 30.0, 15.0, 15.0, ),
      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0, ),
      child: Text(
        "Meta Econômica".i18n,
        style: TextStyle(
          color: primaryColor,
          fontSize: 30,
          fontFamily: "Lobster",
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Padding buildBackgroundAppNameShadow() {
    return Padding(
      //padding: const EdgeInsets.fromLTRB(20.0, 35.0, 20.0, 20.0,),
      padding: const EdgeInsets.fromLTRB(7.0, 7.0, 0.0, 0.0,),
      child: Text(
        "Meta Econômica".i18n,
        style: TextStyle(
          color: Colors.white,
          fontSize: 30,
          fontFamily: "Lobster",
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}

class Constants{
  static const PROFILE_LINKEDIN = 'https://www.linkedin.com/in/everson-dias-ferreira/';
  static const PROFILE_GITHUB = 'https://github.com/Fidjis';
  static const PROFILE_BITBUCKET = 'https://bitbucket.org/fidjis';
  static const PROFILE_INSTAGRAM = 'https://www.instagram.com/eversondiasferreira/';
}

class Assets{
  static const avatar = 'assets/avatar.jpg';
  static const facebook = 'assets/facebook.png';
  static const github = 'assets/github.png';
  static const bitbucket = 'assets/bitbucket.png';
  static const instagram = 'assets/instagram.png';
  static const linkedin = 'assets/linkedin.png';
  static const medium = 'assets/medium.png';
}